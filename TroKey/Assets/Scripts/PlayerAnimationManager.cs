﻿using UnityEngine;
using System.Collections;

public class PlayerAnimationManager : MonoBehaviour {

	public Animator animator;
//	private InputState inputState;
	public bool meetEnemies = false;
	//private bool enter = false;
	public Animator EnemySideEffectAnimator;
	public int actionId = 0;

	void Awake(){
		animator = GetComponent<Animator> ();
	}

	void Update () {
		actionId = animator.GetInteger ("action");
		EnemySideEffectAnimator = GameObject.Find ("EffectEnemySide").GetComponent<Animator> ();

		if (meetEnemies == true) {
			animator.SetBool ("fight", true);
		} else {
			animator.SetBool ("fight", false);
			animator.SetInteger ("action", -1);
			return;
		}
		string trokeyAction = GameObject.Find ("GameManager").GetComponent<GameManager> ().trokeyAction;
		string FightState = GameObject.Find ("GameManager").GetComponent<GameManager> ().FightState;
		var done = GameObject.Find ("GameManager").GetComponent<GameManager> ().enemyActionDone;
		var ememyAction = GameObject.Find ("GameManager").GetComponent<GameManager> ().enemyAction;

		//Debug.Log("****************************"+ememyAction);
		if (FightState == "Start") {
			//Debug.Log ("XXXXXXXXXXXXXXXXXXXXXX" + trokeyAction);
		}
		//Debug.Log ("XXXXXXXXXXXXXXXXXXXXXX" + trokeyAction);
		if (trokeyAction == "Attack" && FightState != "Wait") {
			animator.SetInteger ("action", 1);
			EnemySideEffectAnimator.SetInteger ("state", 1);
			return;
		}
		if (trokeyAction == "Defense" && FightState != "Wait") {
			animator.SetInteger ("action", 2);
			EnemySideEffectAnimator.SetInteger ("state", 0);
			return;
		}

		if (trokeyAction == "Ultimate" && FightState != "Wait") {
			animator.SetInteger ("action", 3);
			EnemySideEffectAnimator.SetInteger ("state", 2);
			return;
		}

		if (trokeyAction == "CounterAttack" && FightState != "Wait") {
			animator.SetInteger ("action", 4);
			if (ememyAction == "Attack" || ememyAction == "Ultimate") {
				EnemySideEffectAnimator.SetInteger ("state", 1);
			}
			return;
		}

		//ExecuteAfterTime ();
		if (done == false) {
			animator.SetInteger ("action", 0);
			EnemySideEffectAnimator.SetInteger ("state", 0);

		}
			
	}
	IEnumerator ExecuteAfterTime() {
		yield return new WaitForSeconds(2);
		animator.SetInteger ("action", 0);
		EnemySideEffectAnimator.SetInteger ("state", 0);

	}
}
