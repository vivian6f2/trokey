﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangePauseImg : MonoBehaviour {
	public Sprite pauseBT;
	public Sprite continueBT;
//	GameObject pause;
//	// Use this for initialization
//	void Start () {
//		pause = GameObject.Find("PauseButton");
//	}

//	button = GetComponent<Button>();
	// Update is called once per frame
	public void ChangeImg (Button pause) {
		if (pause.image.sprite == continueBT) {
			pause.image.sprite = pauseBT;
		} else {
			pause.image.sprite = continueBT;
		}
	}	
}
