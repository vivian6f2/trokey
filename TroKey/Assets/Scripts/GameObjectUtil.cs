﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectUtil {

	public static Dictionary<RecycleGameObject, ObjectPool> pools = new Dictionary<RecycleGameObject, ObjectPool> ();

	public static GameObject Instantiate(GameObject prefab, Vector3 pos){
		GameObject instance = null;

		var recycledScript = prefab.GetComponent<RecycleGameObject> ();
		if (recycledScript != null) {
			var pool = GetObjectPool (recycledScript);
			instance = pool.NextObject (pos).gameObject;
		} else {
			instance = GameObject.Instantiate (prefab);
			instance.transform.position = pos;
		}

		return instance;
	}

	public static GameObject InstantiateNonActive(GameObject prefab, Vector3 pos){
		GameObject instance = null;

		var recycledScript = prefab.GetComponent<RecycleGameObject> ();
		if (recycledScript != null) {
			var pool = GetObjectPool (recycledScript);
			instance = pool.NextObject (pos).gameObject;
			var recycleGameObject = instance.GetComponent<RecycleGameObject> ();
			//recycleGameObject.Shutdown ();
		} else {
			instance = GameObject.Instantiate (prefab);
			instance.transform.position = pos;
		}

		return instance;
	}

	public static void Destroy(GameObject gameObject){

		var recycleGameObject = gameObject.GetComponent<RecycleGameObject> ();

		if (recycleGameObject != null) {
			recycleGameObject.Shutdown ();
		} else {
			GameObject.Destroy (gameObject);
		}
	}

	public static ObjectPool GetObjectPool(RecycleGameObject reference){
		ObjectPool pool = null;


		if (pools.ContainsKey (reference) && pools[reference] != null) {
			pool = pools [reference];
		}else if(pools.ContainsKey (reference) && pools[reference] == null){
			var poolContainer = new GameObject(reference.gameObject.name + "ObjectPool");
			pool = poolContainer.AddComponent<ObjectPool> ();
			pool.DestroyPoolInstances ();
			pool.prefab = reference;
			pools[reference] = pool;
		}else {
			var poolContainer = new GameObject(reference.gameObject.name + "ObjectPool");
			pool = poolContainer.AddComponent<ObjectPool> ();
			pool.prefab = reference;
			pools.Add (reference, pool);
		}

		return pool;
	}

	public static void DestroyObjectPools(){
		pools = new Dictionary<RecycleGameObject, ObjectPool> ();
	}
}