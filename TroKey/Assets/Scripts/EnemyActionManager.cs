﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActionManager : MonoBehaviour {

//	attack = {
//		attack:{Time:5, power:2},
//		defense:{Time:2},
//		ultimate:{Time:6, power:10},
//		counterattack{Time:5}:
//	}
	public int enemyBloodLevel = 100; 
	public int defaultBloodlevel = 0;
	public bool dead = false;
	public bool fight = false;
	public int AttackTime = 0;
	public int DefenseTime = 0;
	public int UltimateTime = 0;
	public int CounterAttackTime = 0;
	public int AttackPower = 0;
	public int DefensePower = 0;
	public int UltimatePower = 0;
	public int CounterAttackPower = 0;
	public int ActionDelay = 0;
	public Animator animator;
	public Animator trokeyAnimator;
	public Animator TrokeySideEffectAnimator;
	public Animator EnemySideEffectAnimator;
	public Animator EnemyActionIcon;

	private GameObject GameManager;

	public Dictionary<string, Dictionary<string, int>> Actions = 
		new Dictionary<string, Dictionary<string, int>>
		{
			{"Attack", new Dictionary<string, int>{{"Time",0}, {"Power",0}}},
			{"Defense", new Dictionary<string, int>{{"Time",0}, {"Power",0}}},
			{"Ultimate",new Dictionary<string, int>{{"Time",0}, {"Power",0}}},
			{"CounterAttack",new Dictionary<string, int>{{"Time",0}, {"Power",0}}},
		};
	public string Action = "";
	private List<string> ActionList = new List<string>();

	void Awake () {
	}
		

	// Use this for initialization
	void Start () {
		Actions ["Attack"] ["Time"] = AttackTime;
		Actions ["Attack"] ["Power"] = AttackPower;
		Actions ["Defense"] ["Time"] = DefenseTime;
		Actions ["Defense"] ["Power"] = DefensePower;
		Actions ["Ultimate"] ["Time"] = UltimateTime;
		Actions ["Ultimate"] ["Power"] = UltimatePower;
		Actions ["CounterAttack"] ["Time"] = CounterAttackTime;
		Actions ["CounterAttack"] ["Power"] = CounterAttackPower;
		ActionList = new List<string> (Actions.Keys);
		defaultBloodlevel = enemyBloodLevel;
		GameManager = GameObject.Find ("GameManager");
		animator = GetComponent<Animator> ();
		TrokeySideEffectAnimator = GameObject.Find ("EffectTrokeySide").GetComponent<Animator> ();
		EnemySideEffectAnimator = GameObject.Find ("EffectEnemySide").GetComponent<Animator> ();
		EnemyActionIcon = GameObject.Find ("ActionIcon").GetComponent<Animator> ();

	}

	// Update is called once per frame
	void Update () {
		var done = GameObject.Find ("GameManager").GetComponent<GameManager> ().enemyActionDone;
		var	trokeyAction = GameObject.Find ("GameManager").GetComponent<GameManager> ().trokeyAction;
		string FightState = GameObject.Find ("GameManager").GetComponent<GameManager> ().FightState;
		var enemyAction = GameObject.Find ("GameManager").GetComponent<GameManager> ().enemyAction;

		//trokeyAnimator = GameObject.Find ("PlayerActionManager").GetComponent<Animator>();
		//Debug.Log (trokeyAnimator);
		if (enemyAction == "") {
			EnemyActionIcon.SetInteger ("state", 0);
		} else if (enemyAction == "Attack") {
			EnemyActionIcon.SetInteger ("state", 1);
		} else if (enemyAction == "Defense") {
			EnemyActionIcon.SetInteger ("state", 2);
		} else if (enemyAction == "Ultimate") {
			EnemyActionIcon.SetInteger ("state", 3);
		} else if (enemyAction == "CounterAttack") {
			EnemyActionIcon.SetInteger ("state", 4);
		}

		if (Action == "Attack") {
			if (done) {
				animator.SetInteger ("States", 2);
				TrokeySideEffectAnimator.SetInteger ("state", 1);
			
			} else {
//				Debug.Log ("prepare Attack");
				animator.SetInteger ("States", 6);
				TrokeySideEffectAnimator.SetInteger ("state", 0);

			}
		} else if (Action == "Defense") {
			if (done) {
				animator.SetInteger ("States", 3);
			} else {
				animator.SetInteger ("States", 6);

			}
		} else if (Action == "Ultimate") {
			if (done) {
				animator.SetInteger ("States", 4);
				TrokeySideEffectAnimator.SetInteger ("state", 2);

			} else {
//				Debug.Log ("prepare Ultimate");
				animator.SetInteger ("States", 6);
				TrokeySideEffectAnimator.SetInteger ("state", 0);

			}
		} else if (Action == "CounterAttack") {
			 //animator.SetInteger ("States",5);
			//TrokeySideEffectAnimator.SetInteger ("state", 1);
			if (done) {
				animator.SetInteger ("States", 5);
				var trokeyAnimator1 = GameObject.Find ("player").GetComponent<PlayerAnimationManager>().animator;
				int actionId = trokeyAnimator1.GetInteger ("action");
				//Debug.Log ("xxxxxxxxxxxxxxxxxxxxxx" + actionId);
				if (actionId == 1 || actionId == 3) {
					TrokeySideEffectAnimator.SetInteger ("state", 1);
				}

			} else {
				//Debug.Log ("prepare Ultimate");
				animator.SetInteger ("States", 6);
				TrokeySideEffectAnimator.SetInteger ("state", 0);

			}
		} else if(done == false){
			animator.SetInteger ("States", 6);
			TrokeySideEffectAnimator.SetInteger ("state", 0);

				
		}

		if (GameManager.GetComponent<GameManager>().FightState == "Wait") {
			// start sending action to game manager
			if (Action == "") {
				StartCoroutine(ActionGenerator ());
				TrokeySideEffectAnimator.SetInteger ("state", 0);

			}
		}
			

		if (dead == true) {
			animator.SetInteger ("States",-1);
			TrokeySideEffectAnimator.SetInteger ("state", 0);
			EnemySideEffectAnimator.SetInteger ("state", 0);

		}
	}

	IEnumerator ActionGenerator(){
		// random pick a new action and send to GameManager
		Action = ActionList [Random.Range (0, ActionList.Count)];
		GameObject.Find ("GameManager").GetComponent<GameManager> ().enemyAction = Action;
		GameObject.Find ("GameManager").GetComponent<GameManager> ().enemyActionDone = false;

		// wait for action time delay
		yield return new WaitForSeconds (Actions[Action]["Time"]);

		// tell GameManager that the action is done
		GameManager.GetComponent<GameManager> ().enemyActionDone = true;

		// action delay
		yield return new WaitForSeconds (ActionDelay);
		Action = "";
		GameObject.Find ("GameManager").GetComponent<GameManager> ().enemyAction = Action;
		GameManager.GetComponent<GameManager> ().enemyActionDone = false;
	}
}
