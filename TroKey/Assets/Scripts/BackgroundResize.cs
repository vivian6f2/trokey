﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundResize : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var h = Camera.main.orthographicSize * 2.0f;
		var w = h * Screen.width / Screen.height;
		transform.localScale = new Vector3 (w, h, 1);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
