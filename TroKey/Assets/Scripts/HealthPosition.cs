﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPosition : MonoBehaviour {
	public bool isTrokey = false;
	public bool isLabel = false;
	public Vector2 offset = Vector2.zero;
	private bool ChangeSize = false;

	private GameObject[] EnemyPrefabs;
	private GameObject EnemySpawner;


	void Awake(){
	}

	// Use this for initialization
	void Start () {

		//var enemyHeight = objectList [0].GetComponent<SpriteRenderer> ().bounds.size.y;

		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		ChangeSize = true;
		offset.x = isTrokey ? -Mathf.Ceil (0.9f * Screen.width / pixelsToUnits): (isLabel ? -Mathf.Ceil (0.75f * Screen.width / pixelsToUnits) : Mathf.Ceil (0.4f * Screen.width / pixelsToUnits + 32));
		offset.y = isTrokey ? Mathf.Ceil (Screen.height / pixelsToUnits * 0.8f) : (isLabel ? Mathf.Ceil (Screen.height / pixelsToUnits * 0.7f) : 10.0f);

		GetComponent<HealthPosition> ().GetComponent<RectTransform> ().localPosition = new Vector3( offset.x, offset.y, 0);
		GetComponent<HealthPosition> ().GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.15f * (Screen.width / pixelsToUnits), 0.15f * (Screen.width / pixelsToUnits));
		if (!isLabel && !isTrokey) {
			GetComponent<HealthPosition> ().GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.2f * (Screen.width / pixelsToUnits), 0.05f * (Screen.width / pixelsToUnits));
		}
	}

	void Update(){
		EnemySpawner = GameObject.Find ("EnemySpawner");
		EnemyPrefabs = EnemySpawner.GetComponent<Spawner> ().prefabs;
		var recycledScript = EnemyPrefabs[0].GetComponent<RecycleGameObject> ();
		var pool = GameObjectUtil.GetObjectPool (recycledScript);
		var objectList = pool.GetActiveObjects ();
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		var scale = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().scale;
		if (objectList.Count > 0) {
			var enemyHeight = objectList [0].GetComponent<SpriteRenderer> ().bounds.size.y;
			//Debug.Log (enemyHeight);
			offset.x = isTrokey ?-Mathf.Ceil (0.9f * Screen.width / pixelsToUnits): (isLabel ? -Mathf.Ceil (0.75f * Screen.width / pixelsToUnits) : Mathf.Ceil (0.4f * Screen.width / pixelsToUnits));
			offset.y = isTrokey ? Mathf.Ceil (Screen.height / pixelsToUnits * 0.8f) : (isLabel ? Mathf.Ceil (Screen.height / pixelsToUnits * 0.7f) : (-0.8f * Screen.height) / pixelsToUnits  + (float)enemyHeight * 0.7f * scale );

			GetComponent<HealthPosition> ().GetComponent<RectTransform> ().localPosition = new Vector3( offset.x, offset.y, 0);

		}
//		if (ChangeSize) {
//			ChangeSize = false;
//			offset.x = isTrokey ? -Mathf.Ceil (2.0f * Screen.width / PixelPerfectCamera.pixelsToUnits) : (isLabel ? -Mathf.Ceil (1.6f * Screen.width / PixelPerfectCamera.pixelsToUnits) : (1.9f * Screen.width) / PixelPerfectCamera.pixelsToUnits);
//			offset.y = isTrokey ? Mathf.Ceil (Screen.height / PixelPerfectCamera.pixelsToUnits * 2.0f) : (isLabel ? Mathf.Ceil (Screen.height / PixelPerfectCamera.pixelsToUnits * 1.8f) : (-0.7f * Screen.height) / PixelPerfectCamera.pixelsToUnits );
//
//			GetComponent<HealthPosition> ().GetComponent<RectTransform> ().localPosition = new Vector3( offset.x, offset.y, 0);
//			GetComponent<HealthPosition> ().GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.4f * (Screen.width / PixelPerfectCamera.pixelsToUnits), 0.4f * (Screen.width / PixelPerfectCamera.pixelsToUnits));
//			if (!isLabel && !isTrokey) {
//				GetComponent<HealthPosition> ().GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.4f * (Screen.width / PixelPerfectCamera.pixelsToUnits), 0.1f * (Screen.width / PixelPerfectCamera.pixelsToUnits));
//			}
//		}
	}
}
