﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuCtrl : MonoBehaviour {

	public void setPara(string s) {
		ApplicationModel.scene = s;
	}

	public void LoadScene(string sceneName) {
		var objArray = GameObject.Find ("GameManager");

		Debug.Log (objArray);
		SceneManager.LoadScene (sceneName);
	}
}
