﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempoBarBackground : MonoBehaviour {

	public int textureSize = 32;
	public bool scaleHorizontially = true;
	public bool scaleVertically = false;

	//private bool ChangeSize = false;

	// Use this for initialization (After Awake)
	void Start () {
		//var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		var scale = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().scale;
		//ChangeSize = true;
		var newWidth = !scaleHorizontially ? 1 : Mathf.Ceil ( 0.5f * Screen.width / (textureSize * scale));
		var newHeight = !scaleVertically ? 1 : Mathf.Ceil (Screen.height / (textureSize * scale));

		transform.localScale = new Vector3 (newWidth * textureSize, newHeight * textureSize, 1);

		GetComponent<Renderer> ().material.mainTextureScale = new Vector3 (newWidth, newHeight, 1);

	}

	void Update(){
//		if (ChangeSize) {
//			ChangeSize = false;
//			var newWidth = !scaleHorizontially ? 1 : Mathf.Ceil ( 0.5f * Screen.width / (textureSize * PixelPerfectCamera.scale));
//			var newHeight = !scaleVertically ? 1 : Mathf.Ceil (Screen.height / (textureSize * PixelPerfectCamera.scale));
//
//			transform.localScale = new Vector3 (newWidth * textureSize, newHeight * textureSize, 1);
//
//			GetComponent<Renderer> ().material.mainTextureScale = new Vector3 (newWidth, newHeight, 1);
//
//		}
	}
}
