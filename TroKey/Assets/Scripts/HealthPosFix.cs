﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPosFix : MonoBehaviour {

	public Vector2 offset = Vector2.zero;


	// Use this for initialization
	void Start () {
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		GetComponent<HealthPosFix> ().transform.position = new Vector3( Mathf.Ceil(0.3f * Screen.width / pixelsToUnits), Mathf.Ceil(-0.05f * Screen.height / pixelsToUnits), 0);

	}

	void Update(){
		//		if (ChangeSize) {
		//			offset.x = isIcon ? Mathf.Ceil (0.25f * Screen.width / PixelPerfectCamera.pixelsToUnits) : isHitPoint ? Mathf.Ceil (-0.18f * Screen.width / PixelPerfectCamera.pixelsToUnits) : offset.x;
		//
		//			barHeight = Mathf.Ceil (Screen.height / PixelPerfectCamera.pixelsToUnits / 2 * 0.7f);
		//			GetComponent<TempoBarPosition> ().transform.position = new Vector3( 0 + offset.x, barHeight + offset.y, 0);
		//
		//			ChangeSize = false;
		//		}
	}

}

