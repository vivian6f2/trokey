﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	private GameObject[] EnemyPrefabs;
	private GameObject EnemySpawner;
	private GameObject[] TempoPrefabs;
	private GameObject TempoSpawner;
	private float EnemyPositionX = 0f;
	public string GameState = "Start";
	public string FightState = "Start";
	public string trokeyAction = "";
	public string enemyAction = "";
	public bool enemyActionDone = false;
	public static int Score = 0;
	private GameObject TempoBackground;
	private GameObject HitPoint;
	private GameObject TrokeyHealthLabel;
	private GameObject EnemyHealthBar;
	private GameObject EmojiIcon;
	private GameObject EmojiBubble;
	private int[] EmojiIndexList = new int[] {0, 6, 11, 15};
//	private string[] EmojiList = new string[] {"emoji_0", "emoji_1", "emoji_2", "emoji_3", "emoji_7", "emoji_9", "emoji_14", "emoji_16", "emoji_18", "emoji_27", "emoji_29"};
	//private int trokeyHealth;

	//private Animator TrokeySideEffectAnimator;


	private string tmpOutput = "";
	private bool ChangeSize = false;

	void Awake(){
		TempoSpawner = GameObject.Find ("TempoSpawner");
		TempoPrefabs = TempoSpawner.GetComponent<Spawner> ().prefabs;
		TempoBackground = GameObject.Find ("TempoBackground");
		HitPoint = GameObject.Find ("HitPoint");
		TrokeyHealthLabel = GameObject.Find("TroKeyHealthLabel");
		EnemyHealthBar = GameObject.Find ("EnemyHealthIcon");
		EmojiIcon = GameObject.Find("EmojiIcon");
		EmojiBubble = GameObject.Find ("EmojiBubble");
	}

	// Use this for initialization
	void Start () {
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		ChangeSize = true;
		EnemyPositionX = (0.3f * Screen.width) / pixelsToUnits;
		Debug.Log (EnemyPositionX);
		TrokeyHealthLabel.GetComponent<Text> ().text = "x " + GameObject.Find ("player").GetComponent<PlayerActionManager> ().playerBloodLevel;
		EnemyHealthBar.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.1f * (Screen.width / pixelsToUnits), 0f * (Screen.width / pixelsToUnits));
		Score = 0;
	}
	// Update is called once per frame
	void Update () {
		EnemySpawner = GameObject.Find ("EnemySpawner");
		EnemyPrefabs = EnemySpawner.GetComponent<Spawner> ().prefabs;

//		if (ChangeSize) {
//			EnemyPositionX = (0.4f * Screen.width) / PixelPerfectCamera.pixelsToUnits;
//			Debug.Log (EnemyPositionX);
//			TrokeyHealthLabel.GetComponent<Text> ().text = "x " + GameObject.Find ("player").GetComponent<PlayerActionManager> ().playerBloodLevel;
//			EnemyHealthBar.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.4f * (Screen.width / PixelPerfectCamera.pixelsToUnits), 0f * (Screen.width / PixelPerfectCamera.pixelsToUnits));
//
//			ChangeSize = false;
//		}

		var recycledScript = EnemyPrefabs[0].GetComponent<RecycleGameObject> ();
		var pool = GameObjectUtil.GetObjectPool (recycledScript);
		var objectList = pool.GetActiveObjects ();

		var tempoRecycleScript = TempoPrefabs [0].GetComponent<RecycleGameObject> ();
		var tempoPool = GameObjectUtil.GetObjectPool (tempoRecycleScript);
		var tempoObjectList = tempoPool.GetActiveObjects ();



		if (GameState == "Start") {
			// initialize Trokey's health point and game score.
			//trokeyHealth = GameObject.Find ("player").GetComponent<PlayerActionManager> ().playerBloodLevel;
			GameState = "Run";
			Score = 0;
			EmojiIcon.GetComponent<Renderer>().enabled = false;
			EmojiBubble.GetComponent<Renderer>().enabled = false;

		} else if (GameState == "Run") {
			// Detect enemy position to go to fight state.
			if (EnemyPositionDetect ()) {
				GameState = "Fight";
				FightState = "Start";
				EmojiIcon.GetComponent<Renderer>().enabled = false;
				EmojiBubble.GetComponent<Renderer>().enabled = false;

			}
		} else if (GameState == "Fight") {
			if (FightState == "Start") {
				// initialize fight data
				trokeyAction = "";
				enemyAction = "";
				enemyActionDone = false;
				FightState = "Wait";
				tmpOutput = "";
			} else if (FightState == "Wait") {
				//Debug.Log (tmpOutput);
				//Debug.Log (enemyAction);
				if (tmpOutput == "" && enemyAction != "") {
					Debug.Log ("before" + enemyAction);
					tmpOutput = enemyAction;
				}

				if (trokeyAction != "" && enemyAction != "") {
					FightState = "Fight";
				}
				else if (enemyAction != "") {
					if (enemyActionDone) {
						FightState = "Fight";
					} 
				}
			} else if (FightState == "Fight") {
				//Debug.Log ("TroKey: " + trokeyAction);
				//Debug.Log ("Enemy: " + enemyAction);
				enemyActionDone = true;
				var EnemyData = objectList [0].GetComponent<EnemyActionManager> ().Actions;
				var enemyHealth = objectList [0].GetComponent<EnemyActionManager> ().enemyBloodLevel;
				var TrokeyData = GameObject.Find ("player").GetComponent<PlayerActionManager> ().Actions;
				var trokeyHealth = GameObject.Find ("player").GetComponent<PlayerActionManager> ().playerBloodLevel;
				if (trokeyAction == "") {
					// trokey no action
					if (enemyAction == "Attack") {
						//Debug.Log ("Before Attack TroKey's hp - " + trokeyHealth);
						trokeyHealth -= EnemyData [enemyAction] ["Power"];
//						Debug.Log ("TroKey's hp - " + trokeyHealth);
					} else if (enemyAction == "Defense") {
//						Debug.Log ("Enemy stop defense. Nothing happened");
					} else if (enemyAction == "Ultimate") {
//						Debug.Log ("Before Attack TroKey's hp - " + trokeyHealth);
						trokeyHealth -= EnemyData [enemyAction] ["Power"];
//						Debug.Log ("TroKey's hp - " + trokeyHealth);
					} else if (enemyAction == "CounterAttack") {
//						Debug.Log ("Enemy stop counterattack. Nothing happened");
					}

				} else {
					if (enemyAction == "Attack") {
						if (trokeyAction == "Attack") {
//							Debug.Log ("Before Attack Trokey's Health: " + trokeyHealth);
//							Debug.Log ("Before Attack Enemy's Health: " + enemyHealth);
							trokeyHealth -= EnemyData [enemyAction] ["Power"];
							enemyHealth -= TrokeyData [trokeyAction] ["Power"];
//							Debug.Log ("TroKey's hp - enemy's attack power, enemy's hp - TroKey's attack power");
//							Debug.Log ("After Attack Trokey's Health: " + trokeyHealth);
//							Debug.Log ("After Attack Enemy's Health: " + enemyHealth);
							Score += TrokeyData [trokeyAction] ["Power"];

						} else if (trokeyAction == "Defense") {
//							Debug.Log ("Nothing happened");
						} else if (trokeyAction == "Ultimate") {
							
//							Debug.Log ("Before attack TroKey's hp - " + trokeyHealth);
							trokeyHealth -= EnemyData [enemyAction] ["Power"];
//							Debug.Log ("After attack TroKey's hp - " + trokeyHealth);
						} else if (trokeyAction == "CounterAttack") {
//							Debug.Log ("Before attack Enemy's hp" + enemyHealth);
							enemyHealth -= EnemyData [enemyAction] ["Power"];
							Score += EnemyData [enemyAction] ["Power"];
//							Debug.Log ("After attack Enemy's hp" + enemyHealth);
						}
					} else if (enemyAction == "Defense") {
						if (trokeyAction == "Attack") {
//							Debug.Log ("Nothing happened");
						} else if (trokeyAction == "Defense") {
//							Debug.Log ("Nothing happened");
						} else if (trokeyAction == "Ultimate") {
//							Debug.Log ("Before attack Enemy's hp" + enemyHealth);
							enemyHealth -= TrokeyData [trokeyAction] ["Power"];
							Score += TrokeyData[trokeyAction]["Power"];
//							Debug.Log ("After attack Enemy's hp" + enemyHealth);

						} else if (trokeyAction == "CounterAttack") {
//							Debug.Log ("Nothing happened");
						}

					} else if (enemyAction == "Ultimate") {
						if (trokeyAction == "Attack") {
//							Debug.Log ("Before attack Enemy's hp" + enemyHealth);
							enemyHealth -= TrokeyData [trokeyAction] ["Power"];
							Score += TrokeyData[trokeyAction]["Power"];
//							Debug.Log ("After attack Enemy's hp" + enemyHealth);

						} else if (trokeyAction == "Defense") {
//							Debug.Log ("TroKey's hp - enemy's special skill power");
//							Debug.Log ("Before attack TroKey's hp - " + trokeyHealth);
							trokeyHealth -= EnemyData [enemyAction] ["Power"];
//							Debug.Log ("After attack TroKey's hp - " + trokeyHealth);

						} else if (trokeyAction == "Ultimate") {
//							Debug.Log ("TroKey's hp - enemy's special skill power, enemy's hp - TroKey's special skill power");
//							Debug.Log ("Before attack Enemy's hp" + enemyHealth);
//							Debug.Log ("Before attack TroKey's hp - " + trokeyHealth);
							trokeyHealth -= EnemyData [enemyAction] ["Power"];
							enemyHealth -= TrokeyData [trokeyAction] ["Power"];
							Score += TrokeyData[trokeyAction]["Power"];
//							Debug.Log ("After attack TroKey's hp - " + trokeyHealth);
//							Debug.Log ("After attack Enemy's hp" + enemyHealth);

						} else if (trokeyAction == "CounterAttack") {
//							Debug.Log ("Enemy's hp - enemy's special skill power");
//							Debug.Log ("Before attack Enemy's hp" + enemyHealth);
							enemyHealth -= EnemyData [enemyAction] ["Power"];
							Score += EnemyData [enemyAction] ["Power"];
//							Debug.Log ("After attack Enemy's hp" + enemyHealth);

						}

					} else if (enemyAction == "CounterAttack") {
						if (trokeyAction == "Attack") {
//							Debug.Log ("TroKey's hp - TroKey's attack power");
//							Debug.Log ("Before attack TroKey's hp - " + trokeyHealth);
							trokeyHealth -= TrokeyData [trokeyAction] ["Power"];
//							Debug.Log ("After attack TroKey's hp - " + trokeyHealth);

						} else if (trokeyAction == "Defense") {
//							Debug.Log ("Nothing happened");
						} else if (trokeyAction == "Ultimate") {
//							Debug.Log ("TroKey's hp - TroKey's special skill power");
//							Debug.Log ("Before attack TroKey's hp - " + trokeyHealth);
							trokeyHealth -= TrokeyData [trokeyAction] ["Power"];
//							Debug.Log ("After attack TroKey's hp - " + trokeyHealth);
						} else if (trokeyAction == "CounterAttack") {
//							Debug.Log ("Nothing happened");
						}
					}
				}
				var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
				objectList [0].GetComponent<EnemyActionManager> ().enemyBloodLevel = enemyHealth;
				GameObject.Find ("player").GetComponent<PlayerActionManager> ().playerBloodLevel = trokeyHealth;
				TrokeyHealthLabel.GetComponent<Text> ().text = "x " + trokeyHealth;
				EnemyHealthBar.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.1f * (Screen.width / pixelsToUnits)  * ((float)enemyHealth / (float)objectList [0].GetComponent<EnemyActionManager> ().defaultBloodlevel), (0.03f * (Screen.width / pixelsToUnits)));

				GameObject.Find ("Score").GetComponent<Text> ().text = "Score: " + Score;
				Debug.Log ("TroKey: " + trokeyHealth + ", Enemy: " + enemyHealth);

				// add end state later
				trokeyAction = "";
				enemyAction = "";
				tmpOutput = "";
				//enemyActionDone = false;
				FightState = "Wait";

				//change FightState based on enemy/ trokey's death
				if (enemyHealth <= 0) {
					FightState = "End";
				}
				if (trokeyHealth <= 0) {
					GameState = "End";
				}
			} else if (FightState == "End") {
				trokeyAction = "";
				enemyAction = "";
				enemyActionDone = false;
				EnemyDead ();
				int next = EmojiIndexList[Random.Range (0, EmojiIndexList.Length)];
				Object[] emoji = Resources.LoadAll ("emoji");
//<<<<<<< HEAD
				EmojiIcon.GetComponent<SpriteRenderer> ().sprite = (Sprite)emoji[next+1];
//=======
				EmojiIcon.GetComponent<SpriteRenderer> ().sprite = (Sprite)emoji[next + 1];
//>>>>>>> d65f42577e5a1c338d0f98930461716bba55405a
				EmojiIcon.GetComponent<Renderer>().enabled = true;
				EmojiBubble.GetComponent<Renderer>().enabled = true;
				GameState = "Run";
			}
				

		} else if (GameState == "End") {
			Debug.Log ("Game End!!");
		}
	}
	void EnemyDead(){
		var recycledScript = EnemyPrefabs[0].GetComponent<RecycleGameObject> ();
		var pool = GameObjectUtil.GetObjectPool (recycledScript);
		var objectList = pool.GetActiveObjects ();
		objectList [0].GetComponent<EnemyActionManager> ().dead = true;
		objectList [0].GetComponent<EnemyActionManager> ().fight = false;
		objectList [0].GetComponent<InstantVelocity> ().velocity.x = -50;
		GameObject.Find ("player").GetComponent<PlayerAnimationManager> ().meetEnemies = false;
		GameObject.Find ("Background").GetComponent<AnimatedTexture> ().stop = false;
		GameObject.Find ("Foreground").GetComponent<AnimatedTexture> ().stop = false;
	}

	bool EnemyPositionDetect () {
		var recycledScript = EnemyPrefabs[0].GetComponent<RecycleGameObject> ();
		var pool = GameObjectUtil.GetObjectPool (recycledScript);
		var objectList = pool.GetActiveObjects ();
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;

		if (objectList.Count > 0) {
			if (objectList [0].transform.position.x < EnemyPositionX && !objectList[0].GetComponent<EnemyActionManager>().dead) {
				objectList [0].GetComponent<InstantVelocity> ().velocity.x = 0;
				objectList [0].GetComponent<EnemyActionManager> ().fight = true;
				GameObject.Find ("player").GetComponent<PlayerAnimationManager> ().meetEnemies = true;
				GameObject.Find ("Background").GetComponent<AnimatedTexture> ().stop = true;
				GameObject.Find ("Foreground").GetComponent<AnimatedTexture> ().stop = true;
				EnemyHealthBar.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.1f * (Screen.width / pixelsToUnits), 0.03f * (Screen.width / pixelsToUnits));
				return true;
			}

		}
		return false;
	}
}
