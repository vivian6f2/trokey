﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonPosition : MonoBehaviour {
	public Vector2 offset = Vector2.zero;
	public bool isLeft = false;
	public bool isPause = false;

//	private bool ChangeSize = false;

	// Use this for initialization
	void Start () {
//		ChangeSize = true;
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
//		GetComponent<EnemyPosition> ().transform.position = new Vector3( Mathf.Ceil (0.5f * Screen.width / pixelsToUnits + 32) , (-0.19f * Screen.height) / pixelsToUnits, 0);

		offset.x = isLeft ? -Mathf.Ceil (0.9f * Screen.width / pixelsToUnits) : Mathf.Ceil (0.9f * Screen.width / pixelsToUnits);

		offset.y = isPause ? Mathf.Ceil (Screen.height / pixelsToUnits * 0.7f) : Mathf.Ceil ((-0.9f * Screen.height) / pixelsToUnits);
		GetComponent<ButtonPosition> ().GetComponent<RectTransform> ().localPosition = new Vector3( offset.x, offset.y, 0);
		GetComponent<ButtonPosition> ().GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.18f * (Screen.width / pixelsToUnits), 0.18f * (Screen.width / pixelsToUnits));
	}

	void Update(){
//		if (ChangeSize) {
//			ChangeSize = false;
//			offset.x = isLeft ? -Mathf.Ceil (1.8f * Screen.width / PixelPerfectCamera.pixelsToUnits) : Mathf.Ceil (1.8f * Screen.width / PixelPerfectCamera.pixelsToUnits);
//
//			offset.y = - Mathf.Ceil (Screen.height / PixelPerfectCamera.pixelsToUnits * 2.0f);
//			GetComponent<ButtonPosition> ().GetComponent<RectTransform> ().localPosition = new Vector3( offset.x, offset.y, 0);
//			GetComponent<ButtonPosition> ().GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.4f * (Screen.width / PixelPerfectCamera.pixelsToUnits), 0.4f * (Screen.width / PixelPerfectCamera.pixelsToUnits));
//
//		}
	}
}
