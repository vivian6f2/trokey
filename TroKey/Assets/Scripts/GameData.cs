﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[Serializable]
public class TroKeyData{
	public int playerBloodLevel { get; set; }
	public int AttackPower { get; set; }
	public int DefensePower { get; set; }
	public int UltimatePower { get; set; }
	public int CounterAttackPower { get; set; }
	public int Money { get; set; }

	public TroKeyData(){}
	public TroKeyData(int pb, int ap, int dp, int up, int cap, int mny){
		playerBloodLevel = pb;
		AttackPower = ap;
		DefensePower = dp;
		UltimatePower = up;
		CounterAttackPower = cap;
		Money = mny;
	}

	public override string ToString()
	{
		return "TroKey: " + "hp = " + playerBloodLevel 
			+ ", ap = " + AttackPower + ", dp = " + DefensePower
			+ ", up = " + UltimatePower + ", cap = " + CounterAttackPower;
	}
}

public class EnemyData{
	public string enemyPrefab { get; set; }

	public EnemyData(){}
	public EnemyData(string ep){
		enemyPrefab = ep;
	}

	public override string ToString(){
		return "enemy: " + enemyPrefab;
	}
}

public class GameSceneData{
	public string background { get; set; }
	public string foreground { get; set; }
	public List<EnemyData> enemys { get; set; }

	public GameSceneData(){}
	public GameSceneData(string bg, string fg, List<EnemyData> es){
		background = bg;
		foreground = fg;
		enemys = es;
	}

	public override string ToString(){
		string enemyString = "Enemy Prefabs: ";
		foreach (EnemyData enemy in enemys) {
			enemyString += enemy.ToString ();
		}

		return "game scene data: " + "background = " + background
		+ ", foreground = " + foreground + enemyString;
	}
}

public class GameData{
	
	public TroKeyData troKeyData = new TroKeyData{
		playerBloodLevel = 50, 
		AttackPower = 3, 
		DefensePower = 2,
		UltimatePower = 6, 
		CounterAttackPower = 2,
		Money = 1000
	};

	public TroKeyData trokeyDataLoader(){
		if (File.Exists (Application.persistentDataPath + "/trokeyData.sav")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream stream = new FileStream (Application.persistentDataPath + "/trokeyData.sav", FileMode.Open);

			TroKeyData data = bf.Deserialize (stream) as TroKeyData;
			stream.Close ();
			return data;
		} else {
			return new TroKeyData{
				playerBloodLevel = 50, 
				AttackPower = 3, 
				DefensePower = 2,
				UltimatePower = 6, 
				CounterAttackPower = 2,
				Money = 1000
			};
		}
	}

	public void trokeyDataSaver(TroKeyData data){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream stream = new FileStream (Application.persistentDataPath + "/trokeyData.sav", FileMode.Create);
		bf.Serialize (stream, data);
		stream.Close ();
	}

	public void trokeyDataInit(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream stream = new FileStream (Application.persistentDataPath + "/trokeyData.sav", FileMode.Create);
		bf.Serialize (stream, new TroKeyData{
			playerBloodLevel = 50, 
			AttackPower = 3, 
			DefensePower = 2,
			UltimatePower = 6, 
			CounterAttackPower = 2,
			Money = 1000
		});
		stream.Close ();
	}

	public Dictionary<string, GameSceneData> gameSceneData = 
		new Dictionary<string, GameSceneData> {
		{"desert", new GameSceneData{
				background = "GameBackground-desert",
				foreground = "GameForeground-desert",
				enemys = new List<EnemyData> (
					new EnemyData[]{
						new EnemyData{
							enemyPrefab = "RockMonster"
						}
					}
				)
			}
		},
		{"iceland", new GameSceneData{
				background = "GameBackground-iceland",
				foreground = "GameForeground-iceland",
				enemys = new List<EnemyData> (
					new EnemyData[]{
						new EnemyData{
							enemyPrefab = "IceWolf"
						}
					}
				)
			}
		},
		{"night", new GameSceneData{
				background = "GameBackground-night",
				foreground = "GameForeground-night",
				enemys = new List<EnemyData> (
					new EnemyData[]{
						new EnemyData{
							enemyPrefab = "Evil"
						}
					}
				)
			}
		},
		{"forest", new GameSceneData{
				background = "GameBackground-jungle",
				foreground = "GameForeground-jungle",
				enemys = new List<EnemyData> (
					new EnemyData[]{
						new EnemyData{
							enemyPrefab = "JungleMonster"
						}
					}
				)
			}
		}
	};

	public override string ToString(){
		string gameSceneDataString = "Game Scene Data: ";
		foreach (KeyValuePair<string, GameSceneData> entry in gameSceneData) {
			gameSceneDataString += entry.Key + ": " + entry.Value.ToString ();
		}

		return troKeyData.ToString () + ", " + gameSceneDataString;
	}
}

