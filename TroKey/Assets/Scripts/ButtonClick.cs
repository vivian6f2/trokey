﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClick : MonoBehaviour {
	public bool woo = false;
	public bool pause = false;
	public bool isPause = true;

	// Use this for initialization
	void Start () {
		Button btn = GetComponent<Button> ();
		btn.onClick.AddListener(ButtonClicked);
	}

	void ButtonClicked (){
		if (pause) {
			if (isPause) {
				Time.timeScale = 0;
				GameObject.Find ("Super Mario Bros NES Overworld Theme themagicofthe8bit").GetComponent<AudioSource> ().Pause();
				isPause = false;
				GameObject.Find ("Controller").GetComponent<VisualizerExample> ().rhythmTool.Pause ();
			} else {
				Time.timeScale = 1;
				GameObject.Find ("Super Mario Bros NES Overworld Theme themagicofthe8bit").GetComponent<AudioSource> ().Play();
				isPause = true;
				GameObject.Find ("Controller").GetComponent<VisualizerExample> ().rhythmTool.Play ();
			}
		} else if (woo) {
			GameObject.Find ("HitPoint").GetComponent<InputState> ().PressKey ("A");
		} else {
			GameObject.Find ("HitPoint").GetComponent<InputState> ().PressKey ("D");
		}
	}
}
