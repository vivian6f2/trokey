﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmojiBubblePosition : MonoBehaviour {

	public Vector2 offset = Vector2.zero;
	void Start () {
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		GetComponent<EmojiBubblePosition> ().transform.position = new Vector3( (-0.3f * Screen.width) / pixelsToUnits, (-0.1f * Screen.height) / pixelsToUnits, 0);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
