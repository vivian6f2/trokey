﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileBackground : MonoBehaviour {

	public int textureSize = 32;
	public bool scaleHorizontially = true;
	public bool scaleVertically = true;

	//private bool ChangeSize = false;

	// Use this for initialization
	void Start () {
		//ChangeSize = true;
		//var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		var scale = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().scale;
		var newWidth = ! scaleHorizontially ? 1 : Mathf.Ceil (Screen.width / (textureSize * scale));
		var newHeight = ! scaleVertically ? 1 : Mathf.Ceil (Screen.height / (textureSize * scale));

		transform.localScale = new Vector3 (newWidth * textureSize, newHeight * textureSize, 1);

		GetComponent<Renderer> ().material.mainTextureScale = new Vector3 (newWidth, newHeight, 1);
	}

	void Update(){
//		if (ChangeSize) {
//			var newWidth = ! scaleHorizontially ? 1 : Mathf.Ceil (Screen.width / (textureSize * PixelPerfectCamera.scale));
//			var newHeight = ! scaleVertically ? 1 : Mathf.Ceil (Screen.height / (textureSize * PixelPerfectCamera.scale));
//
//			transform.localScale = new Vector3 (newWidth * textureSize, newHeight * textureSize, 1);
//
//			GetComponent<Renderer> ().material.mainTextureScale = new Vector3 (newWidth, newHeight, 1);
//
//			ChangeSize = false;
//		}
	}

}