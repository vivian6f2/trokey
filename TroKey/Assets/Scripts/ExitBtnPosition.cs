﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitBtnPosition : MonoBehaviour {
	public Vector2 offset = Vector2.zero;

	void Start () {
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		offset.x = Mathf.Ceil (0.0f * Screen.width / pixelsToUnits);

		offset.y = Mathf.Ceil (Screen.height / pixelsToUnits * -0.9f);
		GetComponent<ExitBtnPosition> ().GetComponent<RectTransform> ().localPosition = new Vector3( offset.x, offset.y, 0);
		// GetComponent<ButtonPosition> ().GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.18f * (Screen.width / pixelsToUnits), 0.18f * (Screen.width / pixelsToUnits));
	}

	void Update(){
	}
}


