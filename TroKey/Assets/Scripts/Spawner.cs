﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Spawner : MonoBehaviour {

	public GameObject[] prefabs;
	public float delay = 2.0f;
	public bool active = true;
	public Vector2 delayRange = new Vector2 (1, 2);
	public bool single = false;
	public bool isTempoSpawner = false;
	private IList timeSet = new List<float>();
	private string timeString = "";
	private string[] timeStringArr;
	private float[] timeFloatArr;
	public int counter = 0;
	public float timer = 0.47f;
	private bool played = false;

	private RecycleGameObject recycledScript;
	private ObjectPool pool;
	public float prevTime = 0.0f;
	public float currTime = 0.0f;

	void Awake(){
	}

	// Use this for initialization
	void Start () {
		recycledScript = prefabs[0].GetComponent<RecycleGameObject> ();
		pool = GameObjectUtil.GetObjectPool (recycledScript);
		prevTime = 0.0f;
		currTime = 0.0f;
		counter = 0;
		timer = 0.47f;
//		delay = 2.0f;
		Debug.Log ("check stop restart ");
		Debug.Log ("   ");
		Debug.Log ("   ");
		if (isTempoSpawner) {
			
			timeString = PlayerPrefs.GetString ("SuperM");
			if (timeString == "" || timeString == null) {
				string path = "Assets/Scripts/timeString.txt";
				timeString = File.ReadAllText(path);
			}

			timeStringArr = timeString.Split (' ');
			timeFloatArr = new float[timeStringArr.Length];
			for (int i = 0; i < timeStringArr.Length; i++) {
				float.TryParse (timeStringArr [i], out timeFloatArr [i]);
//				if (i > 0) {
//					timeFloatArr [i] = timeFloatArr [i - 1];
//				}
			}
			Debug.Log ("super size length: " + timeFloatArr.Length);
		} //else {

			ResetDelay ();
			StartCoroutine (ObjectGenerator ());
		//}
//		TempoIconGenerator ();
	}

	void Update() {
		if (timer > 0) {
			timer -= Time.deltaTime;
		} else if(played != true && isTempoSpawner) {
			played = true;
			GameObject.Find("Super Mario Bros NES Overworld Theme themagicofthe8bit").GetComponent<AudioSource>().Play();
		}
//		if (isTempoSpawner) {
//			currTime = Time.time;
//			if (timeFloatArr != null && (currTime) >= timeFloatArr [counter]) {
//				
//				Debug.Log (currTime);
//				Debug.Log (timeFloatArr [counter]);
//				TempoIconGenerator ();
//				counter++;
//				if (counter == timeFloatArr.Length) {
//					counter = 0;
//				}
//			}
//		}
	}

	void TempoIconGenerator(){
		if (active) {
			var newTransform = transform;
			GameObjectUtil.Instantiate (prefabs [Random.Range (0, prefabs.Length)], newTransform.position);
			ResetDelay ();
		}

	}

	IEnumerator ObjectGenerator(){
		yield return new WaitForSeconds (delay);
		if (isTempoSpawner) {
			//Debug.Log (counter +" arrLength:" + timeFloatArr.Length);
		}

		if (active) {
			if (delay >= 0) {
				if (single) {
					var objectList = pool.GetActiveObjects ();
					if (objectList.Count < 1) {
						var newTransform = transform;

						GameObjectUtil.Instantiate (prefabs [Random.Range (0, prefabs.Length)], newTransform.position);
					}
				} else {
					var newTransform = transform;

					GameObjectUtil.Instantiate (prefabs [Random.Range (0, prefabs.Length)], newTransform.position);
				}
				if (isTempoSpawner) {
					currTime = Time.time;
					//Debug.Log ("counter: " + counter);
					//Debug.Log ("actual delay: " + (currTime - prevTime));
					//Debug.Log ("delay: " + delay);

					timeFloatArr [counter] = timeFloatArr [counter] - (currTime - prevTime - delay);

					prevTime = currTime;
				}

			}
			ResetDelay ();
		}


		StartCoroutine (ObjectGenerator ());

	}

	void ResetDelay(){
		if (isTempoSpawner) {
			delay = timeFloatArr [counter++];
			if (counter == timeFloatArr.Length) {
				counter = 0;
			}
		} else {
			delay = Random.Range (delayRange.x, delayRange.y);
		}

	}
}