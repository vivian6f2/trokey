﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForegroundPosition : MonoBehaviour {

	public Vector2 offset = Vector2.zero;
	// private float barHeight = 0.0f;
	//private bool ChangeSize = false;

	// Use this for initialization
	void Start () {
		//ChangeSize = true;
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		// offset.x = isIcon ? Mathf.Ceil (0.25f * Screen.width / PixelPerfectCamera.pixelsToUnits) : isHitPoint ? Mathf.Ceil (-0.18f * Screen.width / PixelPerfectCamera.pixelsToUnits) : offset.x;
		GetComponent<ForegroundPosition> ().transform.position = new Vector3( 0 , (-0.5f * Screen.height + 32) / pixelsToUnits, 0);

	}

	void Update(){
//		if (ChangeSize) {
//			GetComponent<ForegroundPosition> ().transform.position = new Vector3( 0 , (-0.5f * Screen.height + 32) / PixelPerfectCamera.pixelsToUnits, 0);
//
//			ChangeSize = false;
//		}
	}

}
