﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrokeyEffectPosition : MonoBehaviour {

	public Vector2 offset = Vector2.zero;
	private bool ChangeSize = false;
	// private float barHeight = 0.0f;

	// Use this for initialization
	void Start () {
		ChangeSize = true;
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		// offset.x = isIcon ? Mathf.Ceil (0.25f * Screen.width / PixelPerfectCamera.pixelsToUnits) : isHitPoint ? Mathf.Ceil (-0.18f * Screen.width / PixelPerfectCamera.pixelsToUnits) : offset.x;
		GetComponent<TrokeyEffectPosition> ().transform.position = new Vector3( (-0.3f * Screen.width) / pixelsToUnits, (-0.3f * Screen.height) / pixelsToUnits, 0);
//		Debug.Log ((-0.4f * Screen.width) / pixelsToUnits);
//		Debug.Log ((-0.6f * Screen.height) / pixelsToUnits);
	}

	void Update(){
		//		if (ChangeSize) {
		//			ChangeSize = false;
		//			// offset.x = isIcon ? Mathf.Ceil (0.25f * Screen.width / PixelPerfectCamera.pixelsToUnits) : isHitPoint ? Mathf.Ceil (-0.18f * Screen.width / PixelPerfectCamera.pixelsToUnits) : offset.x;
		//			GetComponent<PlayerPosition> ().transform.position = new Vector3 ((-0.4f * Screen.width) / PixelPerfectCamera.pixelsToUnits, 60, 0);
		//			Debug.Log ((-0.4f * Screen.width) / PixelPerfectCamera.pixelsToUnits);
		//		}
	}

}
