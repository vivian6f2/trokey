﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverSceneLoader : MonoBehaviour {
	private GameData data;
	private GameObject GameManager1;


	void Awake(){
		data = new GameData ();
		data.troKeyData = data.trokeyDataLoader ();
	}
	void Start () {
		GameManager1 = GameObject.Find ("GameManager");
	}

	// Update is called once per frame
	void Update () {
		if (GameManager1.GetComponent<GameManager>().GameState == "End") {
			int score = GameManager.Score;
			GameObjectUtil.DestroyObjectPools ();
			data.troKeyData.Money += score;
			data.trokeyDataSaver (data.troKeyData);
			SceneManager.LoadScene ("GameOver");
		}
	}

}
