﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorePosition : MonoBehaviour {

	public Vector2 offset = Vector2.zero;
	// Use this for initialization
	void Start () {
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
//		offset.x = (1.8f * Screen.width) / pixelsToUnits;
//		offset.y = Mathf.Ceil (Screen.height / pixelsToUnits * 1.8f);
		offset.x = Mathf.Ceil (0.9f * Screen.width / pixelsToUnits);
		offset.y = Mathf.Ceil (Screen.height / pixelsToUnits * 0.7f);

		GetComponent<ScorePosition> ().GetComponent<RectTransform> ().localPosition = new Vector3( offset.x, offset.y, 0);
		GetComponent<ScorePosition> ().GetComponent<RectTransform> ().sizeDelta = new Vector2 (1f * (Screen.width / pixelsToUnits), 0.4f * (Screen.width / pixelsToUnits));


		//GetComponent<HealthPosition> ().GetComponent<RectTransform> ().localPosition = new Vector3( offset.x, offset.y, 0);



//		GetComponent<HealthPosition> ().GetComponent<RectTransform> ().localPosition = new Vector3( offset.x, offset.y, 0);
//		GetComponent<HealthPosition> ().GetComponent<RectTransform> ().sizeDelta = new Vector2 (0.4f * (Screen.width / pixelsToUnits), 1f * (Screen.width / pixelsToUnits));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
