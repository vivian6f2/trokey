﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionManager : MonoBehaviour {
	public int playerBloodLevel = 100;
	public int AttackPower = 0;
	public int DefensePower = 0;
	public int UltimatePower = 0;
	public int CounterAttackPower = 0;

	public Dictionary<string, Dictionary<string, int>> Actions = 
		new Dictionary<string, Dictionary<string, int>>
	{
		{"Attack", new Dictionary<string, int>{{"Time",0}, {"Power",0}}},
		{"Defense", new Dictionary<string, int>{{"Time",0}, {"Power",0}}},
		{"Ultimate",new Dictionary<string, int>{{"Time",0}, {"Power",0}}},
		{"CounterAttack",new Dictionary<string, int>{{"Time",0}, {"Power",0}}},
	};

	void Awake(){
//		Actions ["Attack"] ["Power"] = AttackPower;
//		Actions ["Defense"] ["Power"] = DefensePower;
//		Actions ["Ultimate"] ["Power"] = UltimatePower;
//		Actions ["CounterAttack"] ["Power"] = CounterAttackPower;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
