﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEffectPosition : MonoBehaviour {

	public Vector2 offset = Vector2.zero;
	void Start () {
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		GetComponent<EnemyEffectPosition> ().transform.position = new Vector3( (0.32f * Screen.width) / pixelsToUnits, (-0.22f * Screen.height) / pixelsToUnits, 0);

	}

	void Update(){

	}

}
