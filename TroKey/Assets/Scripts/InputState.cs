﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InputState : MonoBehaviour {

	public float missThreshold = 32.0f;
	public float hitThreshold = 16.0f;

	public Sprite missSprite;
	public Sprite emptyBanana;
	public Sprite gs;
	public Sprite gl;
	public Sprite ws;
	public Sprite wl;

	public string initIconSize = "s";

	private GameObject[] prefabs;
	private GameObject spawner;
	private List<string> keyPressList = new List<string> ();
	private static float hitPoint = 0.0f;
	private float preTime = 0.0F;
	private float curTime = 0.0F;
	private IList timeSet = new List<float>();
	private string timeString;
	public List<Line> prefabLine;
	public int delayFrames = 0;

	public string userInput = "";
	public List<string> userInputBuffer = new List<string> ();

	void Awake(){
		spawner = GameObject.Find ("TempoSpawner");
		prefabs = spawner.GetComponent<Spawner> ().prefabs;

		prefabLine = GameObject.Find ("Controller").GetComponent<VisualizerExample> ().lines;

	}

	void Start(){
		hitPoint = GetComponent<InputState> ().transform.position.x;
	}

	// Update is called once per frame
	void Update () {
		var recycledScript = prefabs[0].GetComponent<RecycleGameObject> ();
		var pool = GameObjectUtil.GetObjectPool (recycledScript);
	    var objectList = pool.GetActiveObjects ();
		if (delayFrames == 0) {
			GameObject.Find ("HitPoint").GetComponent<SpriteRenderer> ().sprite = emptyBanana;
		} else {
			delayFrames--;
		}

		if (Input.GetKeyDown (KeyCode.A)) {
			PressKey ("A");
		}

		if (Input.GetKeyDown (KeyCode.D)) {
			//Debug.Log ("press d");
			PressKey ("D");
		}


		objectList = pool.GetActiveObjects ();
		objectList.Sort (new MissIconComparator ());
		if (objectList.Count > 0 && objectList [0].GetComponent<TempoIconObject> ().hit == false && (hitPoint - objectList [0].transform.position.x) > missThreshold) {
			objectList [0].GetComponent<TempoIconObject> ().hit = true;
			objectList [0].GetComponent<TempoIconObject> ().miss = true;
			objectList [0].GetComponent<SpriteRenderer> ().sprite = missSprite;
			//userInput = "";
			//userInputBuffer = new List<string> ();
		}
			
		if (Input.GetKeyDown (KeyCode.S)) {
			GameObject.Find ("player").GetComponent<PlayerAnimationManager> ().meetEnemies = true;
			GameObject.Find ("Background").GetComponent<AnimatedTexture> ().stop = true;
			GameObject.Find ("Foreground").GetComponent<AnimatedTexture> ().stop = true;
		}

		if (Input.GetKeyDown (KeyCode.W)) {
			GameObject.Find ("player").GetComponent<PlayerAnimationManager> ().meetEnemies = false;
			GameObject.Find ("Background").GetComponent<AnimatedTexture> ().stop = false;
			GameObject.Find ("Foreground").GetComponent<AnimatedTexture> ().stop = false;
		}


		if (userInputBuffer.Count >= 3) {
			int idx = 0;
			foreach (string str in userInputBuffer)
			{
				if (idx != 3) {
					userInput += str;
					idx++;
				}
			}
			userInputBuffer = new List<string>();
		}

		string state = GameObject.Find ("GameManager").GetComponent<GameManager> ().GameState;
		if (userInput == "WWW") {
			// ATTACK
			if (state == "Fight") {
				GameObject.Find("GameManager").GetComponent<GameManager>().trokeyAction = "Attack";
			}


			//Debug.Log ("Action: Attack");
			//userInputBuffer = new List<string>();
			userInput = "";
			//Debug.Log ("Action: StandBy");

		} else if (userInput == "GGG") {
			// Defense
			if (state == "Fight") {
				GameObject.Find ("GameManager").GetComponent<GameManager> ().trokeyAction = "Defense";
			}
			//Debug.Log ("Action: Defense");
			//userInputBuffer = new List<string>();
			userInput = "";
		} else if (userInput == "WGW" || userInput == "GWG") {
			if (state == "Fight") {
				GameObject.Find ("GameManager").GetComponent<GameManager> ().trokeyAction = "Ultimate";
			}
			//Debug.Log ("Action: ULTIMATE");
			//userInputBuffer = new List<string>();
			userInput = "";
			//Debug.Log ("Action: StandBy");
		} else if (userInput == "WWG" || userInput == "Gww") {
			if (state == "Fight") {
				GameObject.Find ("GameManager").GetComponent<GameManager> ().trokeyAction = "CounterAttack";
			}
			//Debug.Log ("Action: Counter attack");
			//userInputBuffer = new List<string>();
			userInput = "";
			//Debug.Log ("Action: StandBy");
		} else {
			userInput = "";
			// userInputBuffer = new List<string>();
		}

	}

//	public void PressButton(string str) {
//		var recycledScript = prefabs[0].GetComponent<RecycleGameObject> ();
//		var pool = GameObjectUtil.GetObjectPool (recycledScript);
//		var objectList = pool.GetActiveObjects ();
//		PressKey (str, objectList);
//	}

	//void PressKey(string input, List<RecycleGameObject> objectList){
	public void PressKey(string input){
		

		string state = GameObject.Find ("GameManager").GetComponent<GameManager> ().GameState;
		bool ss = GameObject.Find ("Controller").GetComponent<VisualizerExample> ().checkHitStatus ();
		delayFrames = 12;
		if (ss == true) {
			//Debug.Log ("CORRECCT!!!!!");	
			keyPressList.Add (input);
			if (input == "A") {
				if (initIconSize == "s") {
					GameObject.Find ("HitPoint").GetComponent<SpriteRenderer> ().sprite = wl;
					initIconSize = "l";
				} else {
					GameObject.Find ("HitPoint").GetComponent<SpriteRenderer> ().sprite = ws;
					initIconSize = "s";
				}

				if (state == "Fight") {
					userInputBuffer.Add ("W");
				}
			} else if (input == "D") {
				if (initIconSize == "s") {
					GameObject.Find ("HitPoint").GetComponent<SpriteRenderer> ().sprite = gl;
					initIconSize = "l";
				} else {
					GameObject.Find ("HitPoint").GetComponent<SpriteRenderer> ().sprite = gs;
					initIconSize = "s";
				}
				if (state == "Fight") {
					userInputBuffer.Add ("G");
				}
				
			}
		} else {
			GameObject.Find("HitPoint").GetComponent<SpriteRenderer>().sprite = missSprite;
			Debug.Log ("miss!");
			userInputBuffer = new List<string>();
			userInput = "";

		}
	}

	public class TempoIconComparator: IComparer<RecycleGameObject> {

		public int Compare(RecycleGameObject r1, RecycleGameObject r2) {
			return (int)(Mathf.Abs (r1.transform.position.x - hitPoint) - Mathf.Abs (r2.transform.position.x - hitPoint));
		}
	}

	public class MissIconComparator: IComparer<RecycleGameObject> {

		public int Compare(RecycleGameObject r1, RecycleGameObject r2) {
			return (int)(r1.transform.position.x - r2.transform.position.x);
		}
	}

//	void OnApplicationQuit()
//	{
//		Debug.Log ("Superrrrr");
//		timeSet.RemoveAt (0);
//		foreach (float i in timeSet) {
//			timeString += i + " ";
//		}
//		PlayerPrefs.SetString("SuperM", timeString);
//		Debug.Log ("SuperM: " + PlayerPrefs.GetString("SuperM"));
//	}
}