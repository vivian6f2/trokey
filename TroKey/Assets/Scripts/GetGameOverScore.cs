﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetGameOverScore : MonoBehaviour {
	private static int maxScore = 0;
	// Use this for initialization
	void Start () {
		GameObject.Find ("txt_score").GetComponent<Text> ().text = "Score: " + 0;
		GameObject.Find ("txt_maxScore").GetComponent<Text> ().text = "Best Score: " + maxScore;
	}
	
	// Update is called once per frame
	void Update () {
		int score = GameManager.Score;
		if (score > maxScore) {
			maxScore = score;
		}
		GameObject.Find ("txt_score").GetComponent<Text> ().text = "Score: " + score;
		GameObject.Find ("txt_maxScore").GetComponent<Text> ().text = "Best Score: " + maxScore;
	}
}
