﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOffscreen : MonoBehaviour {

	public float offset = 16f;
	public delegate void OnDestroy ();
	public event OnDestroy DestroyCallback;
	public bool isIcon = false;

	private bool offscreen;
	private float offscreenX = 0;
	private Rigidbody2D body2d;
	//private bool ChangeSize = false;

	void Awake(){
		body2d = GetComponent<Rigidbody2D> ();
	}

	// Use this for initialization
	void Start () {
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		//ChangeSize = true;
		offscreenX = isIcon ? (0.25f * (Screen.width / pixelsToUnits)) : (Screen.width / pixelsToUnits) / 2 + offset;
	}

	// Update is called once per frame
	void Update () {
//		if (ChangeSize) {
//
//			ChangeSize = false;
//			offscreenX = isIcon ? (0.25f * (Screen.width / PixelPerfectCamera.pixelsToUnits)) : (Screen.width / PixelPerfectCamera.pixelsToUnits) / 2 + offset;
//
//		}

		var posX = transform.position.x;
		var dirX = body2d.velocity.x;

		if (Mathf.Abs (posX) > offscreenX) {
			if (dirX < 0 && posX < -offscreenX) {
				offscreen = true;
			} else if (dirX > 0 && posX > offscreenX) {
				offscreen = true;
			}
		} else {
			offscreen = false;
		}

		if (offscreen) {
			OnOutOfBounds ();
		}
	}

	public void OnOutOfBounds(){
		offscreen = false;
		GameObjectUtil.Destroy (gameObject);

		if (DestroyCallback != null) {
			DestroyCallback ();
		}
	}


}