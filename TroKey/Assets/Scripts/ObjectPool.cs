﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

	public RecycleGameObject prefab;

	private List<RecycleGameObject> poolInstances = new List<RecycleGameObject>();

	private RecycleGameObject CreateInstance(Vector3 pos){

		var clone = GameObject.Instantiate (prefab);
		clone.transform.position = pos;
		clone.transform.parent = transform;

		poolInstances.Add (clone);

		return clone;
	}

	public RecycleGameObject NextObject(Vector3 pos){
		RecycleGameObject instance = null;

		foreach(var go in poolInstances){
			if (go.gameObject.activeSelf != true) {
				instance = go;
				instance.transform.position = pos;
				TempoIconObject tempoIcon = instance.GetComponent<TempoIconObject> ();
				if (tempoIcon) {
					tempoIcon.hit = false;
					tempoIcon.miss = false;
					tempoIcon.GetComponent<SpriteRenderer> ().sprite = tempoIcon.originSprite;
//					var tmpV = tempoIcon.GetComponent<InstantVelocity> ().velocity.x;
					//Debug.Log (tmpV);
				}
				EnemyActionManager enemy = instance.GetComponent<EnemyActionManager> ();
				if (enemy) {
					enemy.dead = false;
					enemy.Action = "";
					enemy.GetComponent<InstantVelocity> ().velocity.x = -50;
					enemy.enemyBloodLevel = enemy.defaultBloodlevel;
				}
			}
		}

		if(instance == null)
			instance = CreateInstance (pos);

		instance.Restart ();

		return instance;
	}

	public List<RecycleGameObject> GetActiveObjects(){
		List<RecycleGameObject> objectList = new List<RecycleGameObject>();

		foreach (var element in poolInstances) {
			if (element.GetComponent<TempoIconObject> ()) {
				if (element.gameObject.activeSelf == true && element.GetComponent<TempoIconObject> ().hit == false) {
					objectList.Add (element);
				}
			} else {
				if (element.gameObject.activeSelf == true) {
					objectList.Add (element);
				}
			}
		}

		return objectList;
	}

	public void DestroyPoolInstances(){
		poolInstances = new List<RecycleGameObject>();
	}
}