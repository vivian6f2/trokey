﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionIconPosition : MonoBehaviour {

	public Vector2 offset = Vector2.zero;
	void Start () {
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		GetComponent<ActionIconPosition> ().transform.position = new Vector3( (0.3f * Screen.width) / pixelsToUnits, (0.05f * Screen.height) / pixelsToUnits, 0);

	}

	void Update(){

	}

}
