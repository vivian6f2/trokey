﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour {

	private Rigidbody2D rb2d;
	public float velocity = -1.5f;

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
		rb2d.velocity = new Vector2 (velocity, 0);
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.LeftArrow)) {
			rb2d.velocity = rb2d.velocity * 0.9f;
		}

		if (Input.GetKey (KeyCode.RightArrow)) {
			rb2d.velocity = rb2d.velocity * 1.2f;
		}

	}
}

