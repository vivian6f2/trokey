﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetTrokeyAttribute : MonoBehaviour {
	private GameData data;
	private Text blood;
	private Text attack;
	private Text defense;
	private Text money;
	private Text blood_mny;
	private Text attack_mny;
	private Text defense_mny;

	void Awake() {
		blood = GameObject.Find ("Txt_blood_num").GetComponent<Text> ();
		attack = GameObject.Find ("Txt_attack_num").GetComponent<Text> ();
		defense = GameObject.Find ("Txt_defense_num").GetComponent<Text> ();
		money = GameObject.Find ("Txt_money_num").GetComponent<Text> ();
		blood_mny = GameObject.Find ("Txt_blood_mny").GetComponent<Text> ();
		attack_mny = GameObject.Find ("Txt_attack_mny").GetComponent<Text> ();
		defense_mny = GameObject.Find ("Txt_defense_mny").GetComponent<Text> ();

		data = new GameData ();
		data.troKeyData = data.trokeyDataLoader ();
		Debug.Log (data);

		blood.text = ""+ data.troKeyData.playerBloodLevel;
		attack.text = ""+ data.troKeyData.AttackPower;
		defense.text = ""+ data.troKeyData.DefensePower;
		money.text = ""+ data.troKeyData.Money;
		attack_mny.text = ""+ data.troKeyData.AttackPower * 10;
		blood_mny.text = ""+ data.troKeyData.playerBloodLevel * 10;
		defense_mny.text = ""+ data.troKeyData.DefensePower * 10;

	}

	void Start () {
//		blood.text = ""+ data.troKeyData.playerBloodLevel;
//		attack.text = ""+ data.troKeyData.AttackPower;
//		defense.text = ""+ data.troKeyData.DefensePower;
	}

	public void save() {
		data.trokeyDataSaver (data.troKeyData);
//		data.trokeyDataInit ();
	}

	public void addAtt() {
		if(data.troKeyData.Money >= data.troKeyData.AttackPower * 10){
			data.troKeyData.AttackPower += 1;
			data.troKeyData.UltimatePower += 1;
			data.troKeyData.CounterAttackPower += 1;
			data.troKeyData.Money -= data.troKeyData.AttackPower * 10;
			attack.text = ""+ data.troKeyData.AttackPower;
			money.text = ""+ data.troKeyData.Money;
			attack_mny.text = ""+ data.troKeyData.AttackPower * 10;
			save ();
		}
	}

	public void addBlood() {
		if(data.troKeyData.Money >= data.troKeyData.playerBloodLevel * 10){
			data.troKeyData.playerBloodLevel += 10;
			data.troKeyData.Money -= data.troKeyData.playerBloodLevel * 10;
			blood.text = ""+ data.troKeyData.playerBloodLevel;
			money.text = ""+ data.troKeyData.Money;
			blood_mny.text = ""+ data.troKeyData.playerBloodLevel * 10;
			save ();
		}
	}

	public void addDef() {
		if(data.troKeyData.Money >= data.troKeyData.DefensePower * 10){
			data.troKeyData.DefensePower += 1;
			data.troKeyData.Money -= data.troKeyData.DefensePower * 10;
			defense.text = ""+ data.troKeyData.DefensePower;
			money.text = ""+ data.troKeyData.Money;
			defense_mny.text = ""+ data.troKeyData.DefensePower * 10;
			save ();
		}
	}
}
