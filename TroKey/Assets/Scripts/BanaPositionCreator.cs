﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class BanaPositionCreator : MonoBehaviour {
	public KeyCode key;
	public bool createMode = true;
	public GameObject banana;
	public IList timeSet = new List<float>();
	private float preTime = 0.0F;
	private float curTime = 0.0F;
	public int counter = 0;
	public String timeString;
	// Use this for initialization
	void Start () {
		string patha = "Assets/Scripts/timeString.txt";
		if (File.Exists (patha)) {
			//Read the text from directly from the test.txt file
//			StreamReader readera = new StreamReader(patha); 
			timeString = File.ReadAllText (patha);
//			Debug.Log(readera);
			Debug.Log("hahahaha" + timeString);
//			readera.Close();
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (createMode) {
			if (Input.GetKeyDown (key)) {
				counter++;
				Instantiate (banana, transform.position, Quaternion.identity);
				curTime = Time.time;
				float timeInterval = curTime - preTime;
				timeSet.Add (timeInterval);

				preTime = curTime;

			}
		}
	}

	void OnApplicationQuit()
	{
		timeSet.RemoveAt (0);
		foreach (float i in timeSet) {
			timeString += i + " ";
		}
		PlayerPrefs.SetString("SuperMario", timeString);
		Debug.Log ("SuperMario: " + PlayerPrefs.GetString("SuperMario"));
	}
}
