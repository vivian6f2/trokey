﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPosition : MonoBehaviour {

	public Vector2 offset = Vector2.zero;
	// private float barHeight = 0.0f;
	private bool ChangeSize = false;

	// Use this for initialization
	void Start () {
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		GetComponent<EnemyPosition> ().transform.position = new Vector3( Mathf.Ceil (0.4f * Screen.width / pixelsToUnits + 32) , (-0.19f * Screen.height) / pixelsToUnits, 0);

	}

	void Update(){
//		if (ChangeSize) {
//			ChangeSize = false;
//			GetComponent<EnemyPosition> ().transform.position = new Vector3( Mathf.Ceil (0.5f * Screen.width / PixelPerfectCamera.pixelsToUnits + 32) , (-0.5f * Screen.height + 64 + 32) / PixelPerfectCamera.pixelsToUnits, 0);
//
//		}
	}
}
