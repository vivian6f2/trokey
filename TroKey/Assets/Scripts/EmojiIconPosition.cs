﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmojiIconPosition : MonoBehaviour {

	public Vector2 offset = Vector2.zero;
	void Start () {
		var pixelsToUnits = GameObject.Find ("Main Camera").GetComponent<PixelPerfectCamera> ().pixelsToUnits;
		GetComponent<EmojiIconPosition> ().transform.position = new Vector3( (-0.3f * Screen.width) / pixelsToUnits, (-0.09f * Screen.height) / pixelsToUnits, -1);

	}

	void Update(){

	}
}
