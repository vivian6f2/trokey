﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetGameSceneData : MonoBehaviour {

	private PlayerActionManager TroKey;
	private Material Background;
	private Material Foreground;
	private Spawner EnemySpawner;
	public string gameSceneName = ApplicationModel.scene;

	void Awake() {
		gameSceneName = ApplicationModel.scene;
		TroKey = GameObject.Find ("player").GetComponent<PlayerActionManager>();
		EnemySpawner = GameObject.Find ("EnemySpawner").GetComponent<Spawner> ();
		GameData data = new GameData ();
		data.troKeyData = data.trokeyDataLoader ();
		Debug.Log (data);

		// update TroKey
		TroKey.playerBloodLevel = data.troKeyData.playerBloodLevel;
		TroKey.AttackPower = data.troKeyData.AttackPower;
		TroKey.DefensePower = data.troKeyData.DefensePower;
		TroKey.UltimatePower = data.troKeyData.UltimatePower;
		TroKey.CounterAttackPower = data.troKeyData.CounterAttackPower;
		TroKey.Actions ["Attack"] ["Power"] = TroKey.AttackPower;
		TroKey.Actions ["Defense"] ["Power"] = TroKey.DefensePower;
		TroKey.Actions ["Ultimate"] ["Power"] = TroKey.UltimatePower;
		TroKey.Actions ["CounterAttack"] ["Power"] = TroKey.CounterAttackPower;

		// update Background
		Background = Resources.Load<Material>(data.gameSceneData[gameSceneName].background);
		GameObject.Find ("Background").GetComponent<Renderer>().material = Background;

		// update Foreground
		Foreground = Resources.Load<Material>(data.gameSceneData[gameSceneName].foreground);
		GameObject.Find ("Foreground").GetComponent<Renderer>().material = Foreground;

		// update Enemy
		List<GameObject> newPrefabs = new List<GameObject>();
		foreach (EnemyData enemy in data.gameSceneData[gameSceneName].enemys) {
			newPrefabs.Add (Resources.Load<GameObject>(enemy.enemyPrefab));
		}
		EnemySpawner.prefabs = newPrefabs.ToArray ();


	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
